import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;


public class Polinom {
	private List<Monom> values;
	private int grad;
	public Polinom() {
		values=new ArrayList<Monom>();
		grad=0;	
	}
    public List<Monom> get_values() {
    	return values;
    }
    public int get_grad() {
    	return grad;
    }
	void add(float coef,int b) {
		Monom m=new Monom(coef,b);
		values.add(m);
		if(m.get_putere()>this.grad&&m.get_indice()!=0)
			this.grad=m.get_putere();
	}
	void add(Monom x) {
		values.add(x);
		if(x.get_putere()>this.grad&&x.get_indice()!=0)
			this.grad=x.get_putere();
	}
	public List<Monom> getvalues()
	{
		return this.values;
	}
	public void citire(String s) {	
		values.removeAll(values);
		this.grad=0;
		Pattern pattern =Pattern.compile("([+-]?[^-+]+)");
		Matcher matcher=pattern.matcher(s);
		while(matcher.find()) {
			String[] coef=matcher.group(1).split("x\\^\\d+\\+?");
			//obtinem indicele dorit
			System.out.println(matcher.group(1));
			String[] parts =matcher.group(1).split("\\^");
			//obtinem puterea dorita
			String a;
			if(parts.length>1)
				a=parts[1];
			else 
				a="0";
			int co1=Integer.parseInt(coef[0]);
			int ex1=Integer.parseInt(a);
			this.add(co1, ex1);
		}
		System.out.println("Polinomul a fost generat cu succes "+this);	
	}

	//suma polinoamelor
	public Polinom sum(Polinom a) {
		Polinom s=new Polinom();
		int i=0,j=0;
		if(a.grad>this.grad)
			s.grad=a.grad;
		else if(a.grad<this.grad)
			s.grad=this.grad;
		while(i<a.values.size()&&j<this.values.size()) {

			//daca primul are exponentul mai mare
			if(a.values.get(i).get_putere()>this.values.get(j).get_putere())
			{	s.add(a.values.get(i));
			i++;
			}
			else if(a.values.get(i).get_putere()<this.values.get(j).get_putere())
			{
				s.add(this.values.get(j));
				j++;
			}
			else
			{   if(this.values.get(j).get_indice()!=-a.values.get(i).get_indice())
			{s.add(a.values.get(i).adunare_monom(this.values.get(j)));}
			i++;
			j++;
			}
		}
		while(i<a.values.size()||j<this.values.size()) {
			if(i<a.values.size()) {
				s.add(a.values.get(i));
				i++;
			}
			if(j<this.values.size()) {
				s.add(this.values.get(j));
				j++;}}
		return s;
	}
	public Polinom dif(Polinom a) {

		Polinom s=new Polinom();
		int i=0,j=0;

		if(a.grad>this.grad)
		{s.grad=a.grad;
		}
		else if(a.grad<this.grad)
		{s.grad=this.grad;
		}
		
		while(i<a.values.size()&&j<this.values.size()) {

			//daca primul are exponentul mai mare
			if(a.values.get(i).get_putere()>this.values.get(j).get_putere())
			{	Monom r=new Monom(-a.values.get(i).get_indice(),a.values.get(i).get_putere());
			s.add(r);

			i++;
			}
			else if(a.values.get(i).get_putere()<this.values.get(j).get_putere())
			{
				s.add(this.values.get(j));
				j++;
			}
			else
			{  
				if(this.values.get(j).get_indice()==a.values.get(i).get_indice())
					;
				else
					s.add(this.values.get(j).scadere_monom(a.values.get(i)));
				i++;
				j++;
			}

		}

		while(i<a.values.size()||j<this.values.size()) {
			if(i<a.values.size()) {
				Monom m1=new Monom(-a.values.get(i).get_indice(),a.values.get(i).get_putere());
				s.add(m1);
				i++;
			}
			if(j<this.values.size()) {
				s.add(this.values.get(j));
				j++;
			}
		}
		return s;
	}

	public Polinom inmultire(Polinom a) {
		Polinom s=new Polinom();
		s.grad=a.grad+this.grad;
		int[] m1 = new int[s.grad+1];


		for(Monom i:a.values)
			for(Monom j:this.values) {
				int pow=i.get_putere()+j.get_putere();
				float coef=i.get_indice()*j.get_indice();
				m1[pow]+=coef;
			}
		for(int i1=s.grad;i1>=0;i1--) {
			if(m1[i1]!=0)
				s.add(m1[i1], i1);
		}
		return s;
	}
	public boolean equals(Polinom b) {
		Polinom a=this;
		int ok=0;
		if(a.grad!=b.grad)
			return false;
		else
		{   for(Monom i:a.values)
			  for(Monom j:this.values)
			if(i.get_indice()!=j.get_indice()||i.get_putere()!=j.get_putere())
		      ok=1;
		}
		if (ok==0)
			return true;
		return true;
	}
	public Polinom invers() {
		Polinom x=this;
		Polinom s=new Polinom();
		for(Monom i:x.values) {
			if(i.get_indice()>0||i.get_indice()<0)
				s.add(-i.get_indice(),i.get_putere());			
		}
		return s;
	}
	public Polinom deepcopy() {
		Polinom s=new Polinom();
		for(Monom i:this.values)
			s.add(i);
		s.grad=this.grad;
		return s;

	}
	public void impartire(Polinom b,Polinom_view view) {
		Polinom zero=new Polinom();
		Polinom reminder=this.deepcopy();	
		Polinom t1=new Polinom();
		while(reminder.values.isEmpty()==false&&reminder.grad>=b.grad)
		{
			float co=reminder.values.get(0).get_indice()/b.values.get(0).get_indice();
			int exp=reminder.values.get(0).get_putere()-b.values.get(0).get_putere();
			Monom t=new Monom(co,exp);
			zero.add(t);   
			t1=reminder.dif(t.produs_monom(b));
			reminder.grad=t1.grad;
			reminder=t1;

		}
		
		 view.a1.append("Catul= "+zero+"\n");
		 view.a1.append("Restul= "+reminder);
    
	}
	public Polinom derivare() {
		Polinom s=new Polinom();
		Polinom a=this;
		if(a.values.isEmpty()==true)
			s.add(0,0);
		else
		{
			for(Monom i:a.values) {
				if(i.get_putere()==0)
					s.add(0,0);
				else
					s.add(i.get_indice()*i.get_putere(),i.get_putere()-1);
			}
		}

		return s;
	}
	public Polinom integrare() {
		Polinom s=new Polinom();
		Polinom a=this;
		if(a.values.isEmpty()==true)
			s.add(1,0);
		else
		{
			for(Monom i:a.values) {
				s.add(i.get_indice()/(i.get_putere()+1),i.get_putere()+1);
			}
		}

		return s;
	}
	public String toString(){
		String result="";
		for(Monom i:values) {
			result+=i.getInfo();

		}
		return result;
	}
}
