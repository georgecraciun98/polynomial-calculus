
public class Monom {
private float indice;
private int putere;

public Monom(float a,int b) {
	indice=a;
	putere=b;
}
public float get_indice() {
return indice;
}
public int get_putere() {
	return putere;
}
public Monom adunare_monom(Monom a) {
	float indice1=a.get_indice()+this.get_indice();
	int putere1=a.putere;
	Monom m=new Monom(indice1,putere1);
	return m;
}
public Monom scadere_monom(Monom a) {
	float indice1=this.get_indice()-a.get_indice();
	int putere1=a.putere;
	Monom m=new Monom(indice1,putere1);
	return m;
}
public Polinom produs_monom(Polinom b) {
	Polinom s=new Polinom();
	for(Monom x:b.get_values()) {
		s.add(this.get_indice()*x.get_indice(),this.get_putere()+x.get_putere());
	}
	return s;

}
public String getInfo() {
	if(indice==0)
	 return "";
	
	if(indice<0)
	return ""+indice+"x^"+putere;
	else 
      if(putere==0)
    	  return "+"+indice;
      else
     return "+"+indice+"x^"+putere;
}
}
