import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Polinom_view extends JFrame{
	
	
	JTextField text1=new JTextField(10);
	JTextField text2=new JTextField(10);
	
	JTextArea a1=new JTextArea(10,10);
	JButton b1=new JButton("Polinom 1");
	JButton b2=new JButton("Polinom 2");
	JButton b3=new JButton("  +  ");
	JButton b4=new JButton("  -  ");
	JButton b5=new JButton("  *  ");
	JButton b6=new JButton("  /  ");
	JButton b7=new JButton("  x' ");
	JButton b8=new JButton("  $x  ");
    private Polinom_model model;
    
    Polinom_view(Polinom_model m){
    	JFrame frame=new JFrame("Operatii cu  polinoame");
    	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	frame.setSize(350, 350);
    	GridLayout grid=new GridLayout(6,2,8,6);
    	model=m;
    	JPanel p1=new JPanel();
    	p1.setLayout(grid);
    	p1.add(b1);
    	p1.add(b2);
    	p1.add(b3);
    	p1.add(b4);
    	p1.add(b5);
    	p1.add(b6);
    	p1.add(b7);
    	p1.add(b8);
    	p1.add(text1);
    	p1.add(text2);
    	p1.add(a1);
    	frame.setContentPane(p1);
		frame.setVisible(true);

    }
    void addb1Listener(ActionListener x)
    {
    	b1.addActionListener(x);
    }
    void addb2Listener(ActionListener x)
    {
    	b2.addActionListener(x);
    }
    void addb3Listener(ActionListener x)
    {
    	b3.addActionListener(x);
    }
    void addb4Listener(ActionListener x)
    {
    	b4.addActionListener(x);
    }
    void addb5Listener(ActionListener x)
    {
    	b5.addActionListener(x);
    }
    void addb6Listener(ActionListener x)
    {
    	b6.addActionListener(x);
    }
    void addb7Listener(ActionListener x)
    {
    	b7.addActionListener(x);
    }
    void addb8Listener(ActionListener m)
    {
    	b8.addActionListener(m);
    }
    
	
}
