import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.*;

public class JUnitTest {

	private static Polinom a;
	private static Polinom b;
	private static Polinom s;
	
	private static int nrTesteExecutate = 0;
	private static int nrTesteCuSucces = 0;
	
	public JUnitTest()
	{
		System.out.println("Constructor inaintea fiecarui test!");
	}
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("O singura data inaintea executiei setului de teste din clasa!");
		a = new Polinom();
		b= new Polinom();
		s=new Polinom();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("O singura data dupa terminarea executiei setului de teste din clasa!");
		System.out.println("S-au executat " + nrTesteExecutate + " teste din care "+ nrTesteCuSucces + " au avut succes!");
	}

	@Before
	public void setUp() throws Exception {
		System.out.println("Incepe un nou test!");
		nrTesteExecutate++;
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("S-a terminat testul curent!");
	}

	@Test
	public void test_sum() {
		a.citire("3x^2+2x^1");
		b.citire("2x^2+1x^1");
		s.citire("5x^2+3x^1");
		assertEquals(a.sum(b).toString(),s.toString());
		nrTesteCuSucces++;
	}
	@Test
	public void test_dif() {
		
		a.citire("3x^5+2x^3");
	
		b.citire("2x^2+1x^1");
		s.citire("3x^5+2x^3-2x^2-1x^1");
		assertEquals(a.dif(b).toString(),s.toString());
		nrTesteCuSucces++;
	}
	@Test
    public void test_inmultire() {
		
		a.citire("3x^5+2x^3");
	
		b.citire("2x^2+1x^1");
		s.citire("6x^7+3x^6+4x^5+2x^4");
		assertEquals(a.inmultire(b).toString(),s.toString());
		nrTesteCuSucces++;
	}
	@Test
    public void test_derivare() {
		
		a.citire("3x^5+2x^3");
		s.citire("15x^4+6x^2");
		assertEquals(a.derivare().toString(),s.toString());
		nrTesteCuSucces++;
	}
	@Test
    public void test_integrare() {
		
		a.citire("6x^5+4x^3");
		s.citire("1x^6+1x^4");
		System.out.println(s);
		assertEquals(a.integrare().toString(),s.toString());
		nrTesteCuSucces++;
	}
	
}
