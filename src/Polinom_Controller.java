import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Polinom_Controller {
private Polinom_model model;
private Polinom_view view;

Polinom_Controller(Polinom_model m,Polinom_view v){
	model=m;
	view=v;
	view.addb1Listener(new b1Listener());
	view.addb2Listener(new b2Listener());
	view.addb3Listener(new b3Listener());
	view.addb4Listener(new b4Listener());
	view.addb5Listener(new b5Listener());
	view.addb6Listener(new b6Listener());
	view.addb7Listener(new b7Listener());
	view.addb8Listener(new b8Listener());
}

class b1Listener implements ActionListener{
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		int ok=0;
		String s=view.text1.getText();
		try {
			model.x.citire(s);
			ok=1;
			
		}
		catch(Exception e1) {
			view.a1.setText("Mai incercati o data");
		}
		if(ok==1)
		view.a1.setText(model.x.toString());
}
}
class b2Listener implements ActionListener{
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		int ok=0;
		String s=view.text2.getText();
		try {
			model.y.citire(s);
			ok=1;
		}
		catch(Exception e1) {
			view.a1.setText("Mai incercati o data");
		}
		if(ok==1)
		view.a1.setText(model.y.toString());
	}
}

class b3Listener implements ActionListener{
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		Polinom s=new Polinom();
		view.a1.setText("Suma polinoamelor este\n");
	 if(model.x.get_values().isEmpty()==true)
	    if(model.y.get_values().isEmpty()==true)
	    	view.a1.append("0");
	    else
	    	view.a1.append(model.y.toString());
	 else
		 if(model.y.get_values().isEmpty()==true)
			 view.a1.append(model.x.toString());
		 else
			{ s= model.x.sum(model.y);
			view.a1.append(s.toString());}
	}
}
class b4Listener implements ActionListener{
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		view.a1.setText("Diferenta polinoamelor este\n");	
	 if(model.x.get_values().isEmpty()==true)
	    if(model.y.get_values().isEmpty()==true)
	    	view.a1.append("0");
	    else
	    	view.a1.append(model.y.invers().toString());
	 else
		 if(model.y.get_values().isEmpty()==true)
			 view.a1.append(model.x.toString());
		 else
			{

			 view.a1.append(model.x.dif(model.y).toString());}
	}
}
class b5Listener implements ActionListener{
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		view.a1.setText("Produsul polinoamelor este\n");
	 if(model.x.get_values().isEmpty()==true||model.y.get_values().isEmpty()==true)
	  
		 view.a1.append("0");
	 else
		 view.a1.append(model.x.inmultire(model.y).toString());
	}
	
}
class b6Listener implements ActionListener{
	public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
		view.a1.setText("Rezultatul impartirii este \n");
		model.x.impartire(model.y,view);
	
			}
}
class b7Listener implements ActionListener{
	public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
		view.a1.setText("Rezultatul derivarii primului polinom\n");
		view.a1.append(model.x.derivare().toString());
			}
}
class b8Listener implements ActionListener{
	public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
		view.a1.setText("Rezultatul integrarii primului polinom este\n");
		view.a1.append(model.x.integrare().toString());
             
			}
}
}
